import React,{Fragment,useState, useEffect} from 'react';
import Error from './components/Error';
import Formulario from './components/Formulario';
import Cancion from './components/Cancion';
import Info from './components/Info';
/* import axios from 'axios'; */



function App() {

  //state que comunica el formulario con el app.js
  const [busuqedaletra, guardarBusquedaLetra]=useState({});

  //state para la consulta de la api de letra
  const [letra, guardarLetra]=useState('');

  //state para la consulta de la api de informacion del artista

  const [info, guardarInfo] = useState({});

  //state de para el error de letra
  const [error,guardarError]= useState(false);

  //state para el error de la información del artista
  const [errorInfo,guardarErrorIfo] = useState (false);

  //extraemos las variables de buesqueda letra
  const {artista,cancion}=busuqedaletra;

  useEffect(() => {
   if(Object.keys(busuqedaletra).length===0)return;

   const consultarAPIletra= async()=>{     

    const url=`https://api.lyrics.ovh/v1/${artista}/${cancion}`;
    const url2=`https://theaudiodb.com/api/v1/json/1/search.php?s=coldplay&s=${artista}`
     
    // Premsa para que espere al cargar ambas apis
    //Se puede usar para el await de axios
    const [resultado, resultado2]= await Promise.all([
      fetch(url),
      fetch(url2)
     ]);

    const [letra, informacion]= await Promise.all([
      resultado.json(),
      resultado2.json()
     ]);

     /* console.log('esta letra',letra.lyrics);
     console.log('esta info',informacion); */
    
    // marca un mensaje de error si no encuentra al artista a la informacion del artista
     if(letra.lyrics===undefined){
      guardarError(true);
      return;
      }
      else{guardarError(false);guardarLetra(letra.lyrics);}

      if (informacion.artists===null){guardarErrorIfo(true)}
      else{guardarErrorIfo(false);guardarInfo(informacion.artists[0]);}
   }
   consultarAPIletra();
  }, [busuqedaletra,artista,cancion]);


  return (
    <Fragment>
      <Formulario
        guardarBusquedaLetra={guardarBusquedaLetra}
      />
      {error 
        ? <Error mensaje={`No se encontró la letra de "${cancion}" del artista "${artista}"`} />
        : null}
      {errorInfo 
        ? <Error mensaje={`No se encontró información del artista "${artista}"`} />
        : null}
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-6">
            <Info
              info={info}
            />
          </div>
          <div className="col-md-6">
            <Cancion
                letra={letra}
            />
          </div>
        </div>
      </div>
    </Fragment>
    )
}

export default App;
